# PocketLifestyleCommon

[![CI Status](http://img.shields.io/travis/Wolfgang Damm/PocketLifestyleCommon.svg?style=flat)](https://travis-ci.org/Wolfgang Damm/PocketLifestyleCommon)
[![Version](https://img.shields.io/cocoapods/v/PocketLifestyleCommon.svg?style=flat)](http://cocoapods.org/pods/PocketLifestyleCommon)
[![License](https://img.shields.io/cocoapods/l/PocketLifestyleCommon.svg?style=flat)](http://cocoapods.org/pods/PocketLifestyleCommon)
[![Platform](https://img.shields.io/cocoapods/p/PocketLifestyleCommon.svg?style=flat)](http://cocoapods.org/pods/PocketLifestyleCommon)

## Usage

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

PocketLifestyleCommon is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PocketLifestyleCommon"
```

## Author

Wolfgang Damm, wolfgang@pocket-lifestyle.com

## License

PocketLifestyleCommon is available under the MIT license. See the LICENSE file for more info.
