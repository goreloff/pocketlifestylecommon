//
//  CenteringCollectionViewFlowLayout.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 29/07/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


class CenteringCollectionViewFlowLayout: UICollectionViewFlowLayout {
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return super.shouldInvalidateLayout(forBoundsChange: newBounds) ||  ((newBounds.width >= collectionViewContentSize.width) != needsCentering())
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        let attributes = super.layoutAttributesForElements(in: rect)
        if !needsCentering() {
            return attributes
        }
        
        let deltaX = ((collectionView?.bounds.width ?? 0) - collectionViewContentSize.width) / 2
        
        for (_, a) in (attributes ?? []).enumerated() {
            let attr = a 
            attr.frame.origin.x += deltaX
        }
        
        return attributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attributes = super.layoutAttributesForItem(at: indexPath) else {
            return nil
        }
        
        if !needsCentering() {
            return attributes
        }
        
        let deltaX = ((collectionView?.bounds.width ?? 0) - collectionViewContentSize.width) / 2
        attributes.frame.origin.x += deltaX
        
        return attributes
    }

    fileprivate func needsCentering() -> Bool {
        return collectionView?.bounds.width >= collectionViewContentSize.width
    }
}
