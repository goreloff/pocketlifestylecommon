//
//  PLCaptiveNetworkWrapper.h
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 02/03/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>

@interface PLCaptiveNetworkWrapper : NSObject
+ (nullable NSString*) currentSSID;
+ (nullable NSString*) currentBSSID;
+ (nonnull NSString*) PL_kCMSampleAttachmentKey_DisplayImmediately;
@end
