//
//  PocketLifestyleLib.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 04/02/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import Foundation

public func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}


// MARK: - Typed Notification Observer (http://www.objc.io/snippets/16.html)
private class Box<A> {
    fileprivate let value : A;
    init(_ value: A) {
        self.value = value;
    }
    
    var unbox : A {
        return self.value;
    }
}

public struct BasicNotification {
    let name: String
    public init(name: String) {
        self.name = name
    }
}

public struct NotificationWithoutUserInfo {
    let name: String
}

public struct Notification<A> {
    let name: String
    public init(name: String) {
        self.name = name
    }
}

public func postNotification<A>(_ note: Notification<A>, value: A) {
    let userInfo = ["value": Box(value)]
    let center = NotificationCenter.default
    center.post(name: Foundation.Notification.Name(rawValue: note.name), object: nil, userInfo: userInfo)
}

open class NotificationObserver {
    let observer: NSObjectProtocol
    
    public init<A>(notification: Notification<A>, block aBlock: @escaping (A) -> ()) {
        let center = NotificationCenter.default
        observer = center.addObserver(forName: NSNotification.Name(rawValue: notification.name),
            object: nil,
            queue: nil) { note in
                if let value =  ((note as NSNotification).userInfo?["value"] as? Box<A>)?.unbox {
                    aBlock(value)
                } else if let value = (note as NSNotification).userInfo as? A {
                    aBlock(value)
                }
        }
    }
    
    public init(notification: NotificationWithoutUserInfo, block aBlock: @escaping () -> ()) {
        let center = NotificationCenter.default
        observer = center.addObserver(forName: NSNotification.Name(rawValue: notification.name),
            object: nil,
            queue: nil) { note in
                aBlock()
        }
    }
    
    public init(notification: BasicNotification, blockWithNotification aBlock: @escaping (_ notification: Foundation.Notification) -> ()) {
        let center = NotificationCenter.default
        observer = center.addObserver(forName: NSNotification.Name(rawValue: notification.name),
            object: nil,
            queue: nil) { note in
                aBlock(note)
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(observer)
    }
    
}

open class BackgroundNotificationObserver {
    fileprivate let resignActiveObserver: NotificationObserver
    fileprivate let willEnterForegroundObserver: NotificationObserver
    
    public init(willResignActive: @escaping () -> (), willEnterForeground: @escaping ()->() ) {
        resignActiveObserver = NotificationObserver(notification: NotificationWithoutUserInfo(name: UIApplication.willResignActiveNotification.rawValue), block: willResignActive)
        willEnterForegroundObserver  = NotificationObserver(notification: NotificationWithoutUserInfo(name: UIApplication.didBecomeActiveNotification.rawValue), block: willEnterForeground)
    }
}


open class KeyboardNotificationObserver {
    fileprivate let keyboardWasShownObserver: NotificationObserver
    fileprivate let keyboardWillBeHiddenObserver: NotificationObserver
    
    public init(keyboardWasShown: @escaping (NSDictionary) -> (), keyboardWillBeHidden: @escaping (NSDictionary) -> ()) {
        keyboardWasShownObserver = NotificationObserver(notification: Notification<NSDictionary>(name: UIResponder.keyboardDidShowNotification.rawValue), block: keyboardWasShown)
        keyboardWillBeHiddenObserver = NotificationObserver(notification: Notification<NSDictionary>(name: UIResponder.keyboardWillHideNotification.rawValue), block: keyboardWillBeHidden)
    }
}


// MARK: - Dictionary extension
public func +=<K, V> (left: inout Dictionary<K, V>, right: Dictionary<K, V>) -> Dictionary<K, V> {
    for (k, v) in right {
        left.updateValue(v, forKey: k)
    }
    return left
}
