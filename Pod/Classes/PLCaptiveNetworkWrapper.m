//
//  PLCaptiveNetworkWrapper.m
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 02/03/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

#import "PLCaptiveNetworkWrapper.h"

#import <SystemConfiguration/CaptiveNetwork.h>

@implementation PLCaptiveNetworkWrapper

+ (nullable NSString *)currentSSID {
#if TARGET_IPHONE_SIMULATOR
    return @"MyGoPro";
#else
    NSString *ssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"SSID"]) {
            ssid = info[@"SSID"];
        }
    }
    return ssid;
#endif
}

+ (nullable NSString *)currentBSSID {
#if TARGET_IPHONE_SIMULATOR
    return @"MyGoPro";
#else
    NSString *bssid = nil;
    NSArray *ifs = (__bridge_transfer id)CNCopySupportedInterfaces();
    for (NSString *ifnam in ifs) {
        NSDictionary *info = (__bridge_transfer id)CNCopyCurrentNetworkInfo((__bridge CFStringRef)ifnam);
        if (info[@"BSSID"]) {
            bssid = info[@"BSSID"];
        }
    }
    return bssid;
#endif
}

+ (nonnull NSString*) PL_kCMSampleAttachmentKey_DisplayImmediately {
    return CFBridgingRelease(CFStringCreateCopy(nil, kCMSampleAttachmentKey_DisplayImmediately));
}


@end
