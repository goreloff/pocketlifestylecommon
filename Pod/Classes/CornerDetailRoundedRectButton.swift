//
//  CornerDetailRoundedRectButton.swift
//  Alamofire
//
//  Created by Bahadir Oncel on 5.12.2017.
//

import UIKit

public class CornerDetailRoundedRectButton : RoundedRectButton {

    private var cornerImageView: UIImageView?
    private var cornerImageLabel: UILabel?
    
    public func setCornerDetail(image: UIImage?, title: String){
        cornerImageView = UIImageView(image: image)
        cornerImageLabel = UILabel()
        cornerImageLabel?.text = title
        setupCornerDetail()
    }
    
    private func setupCornerDetail() {
        guard let cornerImageView = cornerImageView, let cornerImageLabel = cornerImageLabel else { return }
        titleLabel?.minimumScaleFactor = 0.5
        titleLabel?.adjustsFontSizeToFitWidth = true
        
        cornerImageView.translatesAutoresizingMaskIntoConstraints = false
        cornerImageView.contentMode = .scaleAspectFit
        
        addSubview(cornerImageView)
        
        cornerImageLabel.translatesAutoresizingMaskIntoConstraints = false
        cornerImageLabel.textColor = UIColor.white
        cornerImageLabel.font = UIFont.boldSystemFont(ofSize: 9)
        cornerImageLabel.transform = CGAffineTransform(rotationAngle: -0.20944)
        cornerImageLabel.numberOfLines = 0
        cornerImageLabel.adjustsFontSizeToFitWidth = true
        cornerImageLabel.minimumScaleFactor = 0.5
        cornerImageLabel.textAlignment = .center
        cornerImageLabel.lineBreakMode = .byWordWrapping
        
        addSubview(cornerImageLabel)
        
        NSLayoutConstraint.activate([
            cornerImageView.topAnchor.constraint(equalTo: topAnchor, constant: 5),
            cornerImageView.heightAnchor.constraint(equalToConstant: cornerImageView.image?.size.height ?? 10.0),
            cornerImageView.widthAnchor.constraint(equalToConstant: cornerImageView.image?.size.width ?? 10.0),
            bottomAnchor.constraint(equalTo: cornerImageView.bottomAnchor, constant: 5),
            cornerImageView.rightAnchor.constraint(equalTo: rightAnchor, constant: -5),
            
            cornerImageLabel.centerXAnchor.constraint(equalTo: cornerImageView.centerXAnchor),
            cornerImageLabel.centerYAnchor.constraint(equalTo: cornerImageView.centerYAnchor),
            
            cornerImageLabel.leftAnchor.constraint(equalTo: cornerImageView.leftAnchor, constant: 5),
            cornerImageLabel.rightAnchor.constraint(equalTo: cornerImageView.rightAnchor, constant: -5),
            cornerImageLabel.topAnchor.constraint(equalTo: cornerImageView.topAnchor, constant: 5),
            cornerImageLabel.bottomAnchor.constraint(equalTo: cornerImageView.bottomAnchor, constant: -5)
            ])
    }
    
    public override func layoutSubviews() {
        let corner = min(bounds.width, bounds.height) / 2
        if cornerImageView != nil {
            contentEdgeInsets = UIEdgeInsets(top: 0, left: corner, bottom: 0, right: bounds.height)
        }else{
            contentEdgeInsets = UIEdgeInsets(top: 0, left: corner, bottom: 0, right: corner)
        }
        super.layoutSubviews()
    }
}
