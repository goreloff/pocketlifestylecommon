//
//  GradientView.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 13/07/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import UIKit

@IBDesignable class GradientView: UIView {
    
    fileprivate var gradientLayer: CAGradientLayer?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup() {
        backgroundColor = UIColor.clear
        gradientLayer = CAGradientLayer()
        gradientLayer?.frame = self.bounds
        gradientLayer?.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer?.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer?.endPoint = CGPoint(x: 0, y: 1)
        self.layer.insertSublayer(gradientLayer!, at: 0)
    }
    
    override func prepareForInterfaceBuilder() {
        setup()
    }
    
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        gradientLayer?.frame = self.bounds
    }
}
