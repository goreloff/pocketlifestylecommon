//
//  TimeEstimator.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 25/08/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import Foundation

@objc public protocol TimeEstimatorDelegate {
    func timeEstimatorChanged(_ sender : TimeEstimator, currentEstimate : Float);
}

open class TimeEstimator: NSObject {
    open weak var delegate : TimeEstimatorDelegate?;
    
    open var estimatedTimeInSeconds : Double {
        get {
            return totalTime - elapsedTime;
        }
    }
    
    fileprivate var firstProgressTime : Date?;
    fileprivate var lastProgress : Double = 0.0;
    fileprivate var totalTime : Double = 0.0;
    fileprivate var elapsedTime : Double = 0.0;
    
    
    open func newProgress(_ progress : Double, now: Date = Date()) {
        if let first = self.firstProgressTime {
            if (self.lastProgress >= progress) {
                // only allow increments
                return;
            }
            self.lastProgress = progress;
            self.elapsedTime = -first.timeIntervalSince(now);
            self.totalTime = self.elapsedTime / progress;
            
            if (self.lastProgress > 0.05) {
                self.delegate?.timeEstimatorChanged(self, currentEstimate: Float(self.estimatedTimeInSeconds));
            }
        } else {
            self.firstProgressTime = now;
        }
    }    
    
    fileprivate func secondsToHMS(_ seconds : Double) -> (hour : Int, minute : Int, second : Int) {
        let hour = Int(seconds / 3600);
        let minute = Int((seconds.truncatingRemainder(dividingBy: 3600))/60);
        let second = Int(seconds.truncatingRemainder(dividingBy: 60));
        
        return (hour, minute, second);
    }
}
