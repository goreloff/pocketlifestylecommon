//
//  ScrollingScreenBreakView.swift
//  Pods
//
//  Created by Wolfgang Damm on 17.12.17.
//

import UIKit

open class ScrollingScreenBreakView: UIScrollView {
    private let contentView: UIView
    private let belowTheBreakView: UIView
    public var breakViewOffset: CGFloat = 0
    
    public init(contentView: UIView, belowTheBreakView: UIView) {
        self.contentView = contentView
        self.belowTheBreakView = belowTheBreakView
        
        super.init(frame: CGRect.zero)
        
        setupUI()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("Unsupported")
        return nil       
    }
    
    public func addAsSubviewAndBound(to view: UIView) {
        view.addSubview(self)
        
        let safeArea: UILayoutGuide
        if #available(iOS 11.0, *) {
            safeArea = view.safeAreaLayoutGuide
        } else {
            safeArea = UILayoutGuide()
            view.addLayoutGuide(safeArea)
            NSLayoutConstraint.activate([
                safeArea.topAnchor.constraint(equalTo: view.topAnchor),
                safeArea.bottomAnchor.constraint(equalTo: view.bottomAnchor),
                safeArea.leftAnchor.constraint(equalTo: view.leftAnchor),
                safeArea.rightAnchor.constraint(equalTo: view.rightAnchor)
                ])
        }
        
        NSLayoutConstraint.activate([
            topAnchor.constraint(equalTo: safeArea.topAnchor),
            bottomAnchor.constraint(equalTo: safeArea.bottomAnchor),
            leftAnchor.constraint(equalTo: safeArea.leftAnchor),
            rightAnchor.constraint(equalTo: safeArea.rightAnchor),
            
            contentView.widthAnchor.constraint(equalTo: safeArea.widthAnchor),
            contentView.heightAnchor.constraint(equalTo: safeArea.heightAnchor, constant: breakViewOffset)
            ])
    }
    
    private func setupUI() {
        contentView.translatesAutoresizingMaskIntoConstraints = false
        belowTheBreakView.translatesAutoresizingMaskIntoConstraints = false
        
        addSubview(contentView)
        addSubview(belowTheBreakView)
        
        NSLayoutConstraint.activate([
            contentView.topAnchor.constraint(equalTo: topAnchor),
            contentView.bottomAnchor.constraint(equalTo: belowTheBreakView.topAnchor),
            contentView.leftAnchor.constraint(equalTo: leftAnchor),
            contentView.rightAnchor.constraint(equalTo: rightAnchor),

            belowTheBreakView.bottomAnchor.constraint(equalTo: bottomAnchor),
            belowTheBreakView.leftAnchor.constraint(equalTo: leftAnchor),
            belowTheBreakView.rightAnchor.constraint(equalTo: rightAnchor)
            ])
    }
}
