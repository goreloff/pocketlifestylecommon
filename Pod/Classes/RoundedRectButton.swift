//
//  RoundedRectButton.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 30/07/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import UIKit

@IBDesignable open class RoundedRectButton: UIButton {

    private var previousHeight: CGFloat = -1.0
    
    @IBInspectable open var borderColor: UIColor? {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    @IBInspectable open var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    open override func prepareForInterfaceBuilder() {
        setup()
    }

    fileprivate func setup() {
        layer.cornerRadius = min(bounds.width, bounds.height) / 2
        layer.borderColor = borderColor?.cgColor
        layer.borderWidth = borderWidth
    }
    
    open override func layoutSubviews() {
        if previousHeight != bounds.height {
            if bounds.height < bounds.width {
                contentEdgeInsets.left += bounds.height / 4
                contentEdgeInsets.right += bounds.height / 4
            }
            previousHeight = bounds.height
        }

        layer.cornerRadius = min(bounds.width, bounds.height) / 2
        super.layoutSubviews()
    }
}
