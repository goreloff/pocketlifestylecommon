//
//  SnappedHeaderFlowLayout.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 14/07/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import UIKit
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}


class SnappedHeaderFlowLayout : UICollectionViewFlowLayout {
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var answer = super.layoutAttributesForElements(in: rect)
        
        let missingSections = NSMutableIndexSet()
        
        var idx = 0
        while idx < answer?.count {
            let layoutAttributes = answer?[idx]
            
            if layoutAttributes?.representedElementCategory == UICollectionView.ElementCategory.cell {
                missingSections.add((layoutAttributes!.indexPath as NSIndexPath).section) // remember that we need to layout header for this section
            }
            if layoutAttributes?.representedElementKind == UICollectionView.elementKindSectionHeader {
                answer?.remove(at: idx) // remove layout of header done by our super, we will do it right later
                idx -= 1
            }
            idx += 1
        }
        
        missingSections.enumerate( { (idx, stop) -> Void in
            let indexPath = IndexPath(item: 0, section: idx)
            if let layoutAttributes =
                self.layoutAttributesForSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, at: indexPath) {
                answer?.append(layoutAttributes)
            }
        })
        
        return answer
    }
    
    override func layoutAttributesForSupplementaryView(ofKind elementKind: String, at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        guard let attributes = super.layoutAttributesForSupplementaryView(ofKind: elementKind, at:indexPath) else {
            return nil
        }
        
        if let cv = collectionView , elementKind == UICollectionView.elementKindSectionHeader {
            let contentOffset = cv.contentOffset
            var nextHeaderOrigin = CGPoint(x: CGFloat.infinity, y: CGFloat.infinity)
            
            if (indexPath as NSIndexPath).section+1 < collectionView?.numberOfSections {
                if let nextHeaderAttributes = super.layoutAttributesForSupplementaryView(ofKind: elementKind, at: IndexPath(item: 0, section: (indexPath as NSIndexPath).section+1)) {
                    nextHeaderOrigin = nextHeaderAttributes.frame.origin
                }
            }
            
            var frame = attributes.frame
            if scrollDirection == .vertical {
                let top = contentOffset.y + cv.contentInset.top
                frame.origin.y = min(max(top, frame.origin.y), nextHeaderOrigin.y - frame.size.height)
            } else { // .Horizontal
                frame.origin.x = min(max(contentOffset.x, frame.origin.x), nextHeaderOrigin.x - frame.size.width)
            }
            
            attributes.zIndex = 1024
            attributes.frame = frame
        }
        return attributes
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true
    }
}
