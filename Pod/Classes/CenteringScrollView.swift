//
//  CenteringScrollView.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 04/08/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import UIKit

open class CenteringScrollView: UIScrollView {
    
    open var shouldCenterVertically = true
    
    open var aspectRatio: CGFloat = 1.0 {
        didSet {
            setNeedsLayout()
        }
    }

    override open func layoutSubviews() {
        super.layoutSubviews()
        
        if let view = delegate?.viewForZooming?(in: self) {
            
            let svw = bounds.size.width
            let svh = bounds.size.height
            
            if aspectRatio <= svh/svw {
                // portrait
                view.bounds.size.width = svw
                view.bounds.size.height = svw * aspectRatio
            } else {
                // landscape
                view.bounds.size.width = svh / aspectRatio
                view.bounds.size.height = svh
            }
            
            let vw = view.frame.size.width
            let vh = view.frame.size.height
            var f = view.frame
            
            if vw < svw {
                f.origin.x = (svw - vw) / 2.0
            } else {
                f.origin.x = 0
            }
            
            if shouldCenterVertically {
                if vh < svh {
                    f.origin.y = (svh - vh) / 2.0
                } else {
                    f.origin.y = 0
                }
            } else {
                f.origin.y = 0
            }
            
            view.frame = f
        }
    }
    
    
    
}
