//
//  PortraitViewControllerForPhonesViewController.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 14/08/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import UIKit

#if !os(tvOS)
open class PortraitViewControllerForPhonesTabBarController : UITabBarController {
    override open var shouldAutorotate : Bool {
        if let navVC = selectedViewController as? UINavigationController {
            return navVC.topViewController?.shouldAutorotate ?? false
        }
        return selectedViewController?.shouldAutorotate ?? super.shouldAutorotate
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if let navVC = selectedViewController as? UINavigationController {
            return navVC.topViewController?.supportedInterfaceOrientations ?? .all
        }
        return selectedViewController?.supportedInterfaceOrientations ?? super.supportedInterfaceOrientations
    }
}

open class PortraitViewControllerForPhonesViewController : UIViewController {
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return shouldAffectRotation() ? UIInterfaceOrientationMask.portrait : super.supportedInterfaceOrientations
    }
}

open class PortraitViewControllerForPhonesTableViewController : UITableViewController {
    override open var shouldAutorotate : Bool {
        return shouldAffectRotation() ? false : super.shouldAutorotate
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return shouldAffectRotation() ? UIInterfaceOrientationMask.portrait : super.supportedInterfaceOrientations
    }
}
    
    
open class PortraitViewControllerForPhonesNavigationController : UINavigationController {
    override open var shouldAutorotate : Bool {
        return shouldAffectRotation() ? false : super.shouldAutorotate
    }
    
    override open var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        return shouldAffectRotation() ? UIInterfaceOrientationMask.portrait : super.supportedInterfaceOrientations
    }
}

private func shouldAffectRotation() -> Bool {
    return UIDevice.current.userInterfaceIdiom != UIUserInterfaceIdiom.pad
}

#endif
