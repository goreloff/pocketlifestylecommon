//
//  CurrentWifiReporter.swift
//  GoProCamsuite
//
//  Created by Wolfgang Damm on 11/05/15.
//  Copyright (c) 2015 pocket-life. All rights reserved.
//

import UIKit

public protocol CurrentWifiReporterDelegate: class {
    // will be nil if device is not connected to any Wifi network
    func currentWifiChanged(_ ssid: String?)
}

open class CurrentWifiReporter: NSObject {
    
    fileprivate struct Constants {
        static let PollingInterval = 2.0
    }
    
    open weak var delegate: CurrentWifiReporterDelegate? {
        didSet {
            delegate?.currentWifiChanged(PLCaptiveNetworkWrapper.currentSSID())
        }
    }
    
    fileprivate var pollingTimer : Timer?
    fileprivate var lastReportedSSID: String?
    
    open func startReporting() {
        pollingTimer?.invalidate()
        pollingTimer = Timer.scheduleWithClosure(repeatInterval: Constants.PollingInterval) { [weak self] (_) -> Void in
            self?.poll()
        }
    }
    
    open func stopReporting() {
        pollingTimer?.invalidate()
    }
    
    
    fileprivate func poll() {
        let ssid = PLCaptiveNetworkWrapper.currentSSID()
        if ssid != lastReportedSSID {
            self.delegate?.currentWifiChanged(ssid)
            lastReportedSSID = ssid
        }
    }
}
