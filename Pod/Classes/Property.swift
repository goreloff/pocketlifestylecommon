//
//  Property.swift
//  Pods
//
//  Created by Wolfgang Damm on 21/09/15.
//
//

import Foundation

open class Property<T> {
    fileprivate var _value: T
    open var value: T {
        get { return _value }
        set {
            _value = newValue
            tellSubscribers()
        }
    }
    
    public init(_ value: T) {
        _value = value
    }
    
    fileprivate var subscriptions = [Subscription<T>]()
    open func subscribe(_ subscriber: AnyObject, next: @escaping (T) -> Void) {
        subscriptions.append(
            Subscription(subscriber: subscriber, next: next))
    }
    
    fileprivate func tellSubscribers() {
        subscriptions =
            subscriptions.compactMap(tellAndFilterSubscription)
    }
    
    fileprivate func tellAndFilterSubscription(_ subscription:
        Subscription<T>) -> Subscription<T>? {
            if subscription.subscriber != nil { // Subscriber exists.
                subscription.next(_value)
                return subscription
            } else { // Subscriber has gone; cull this subscription.
                return nil
            }
    }
}

private struct Subscription<T> {
    weak var subscriber: AnyObject?
    let next: (T) -> Void
}
