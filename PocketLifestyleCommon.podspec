Pod::Spec.new do |s|
  s.name = "PocketLifestyleCommon"
  s.version = "0.2.3"
  s.summary = "A short description of PocketLifestyleCommon."
  s.description = "Common components for PL apps."
  s.homepage = "https://github.com/<GITHUB_USERNAME>/PocketLifestyleCommon"
  s.license = "MIT"
  s.authors = { "Wolfgang Damm" => "wolfgang@pocket-lifestyle.com" }
  
  s.source = { :git => "https://github.com/<GITHUB_USERNAME>/PocketLifestyleCommon.git", :tag => "0.2.3" }
  s.platform = :ios, "11.0"
  s.requires_arc = true
  s.source_files = "Pod/Classes/**/*"

end